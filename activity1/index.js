// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:

console.log("item 1");

let result = (grade) => {
	if (grade <= 74) {
		console.log(`Your grade is ${grade}. Sorry, you failed.`);
	} else if (grade >= 75 && grade <= 80) {
		console.log(`Your grade is ${grade}. You have received a 'Beginner' mark.`);
	} else if (grade >= 81 && grade <= 85) {
		console.log(`Your grade is ${grade}. You have received a 'Developing' mark.`);
	} else if (grade >= 86 && grade <= 90) {
		console.log(`Your grade is ${grade}. You have received an 'Above Average' mark.`);
	} else if (grade >= 91 && grade <= 100) {
		console.log(`Your grade is ${grade}. Congratulations! You have received a 'Advanced' mark.`);
	}
}



result(74);
result(84);
result(90);
result(98);
/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here:
console.log("");
console.log("item 2");
let maxNum = 300;

for (let i=1; i <= maxNum; i++){
	if(i%2 == 0){
		console.log(i + " - even");
	} else if(i%2 == 1){
		console.log(i + " - odd");
	}
}



/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/

//Code here:
console.log("");
console.log("item 3");


let hero = {
	heroName: prompt("Enter the name of your favorite hero."),
	origin: prompt("Enter your hero's place of origin."),
	description: prompt("Describe your hero."),
	skills: {
		skill1: prompt("Enter your hero's 1st Skill"),
		skill2: prompt("Enter your hero's 2nd Skill"),
		skill3: prompt("Enter your hero's 3rd Skill")
	}
}

           
console.log(JSON.stringify(hero))





